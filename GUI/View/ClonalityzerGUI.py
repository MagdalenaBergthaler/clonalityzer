import pandas as pd

from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.sip import delete

from GUI.View.ClonalityzerGui_Designer import Ui_mwClonalityzer
from GUI.View.MplWidget import MplWidget


class TableView(QTableWidget):
    def __init__(self):
        QTableWidget.__init__(self)

        self.setSortingEnabled(True)
        self.setAutoFillBackground(True)
        self.setEditTriggers(QTableWidget.NoEditTriggers)  # data cannot be edited
        # fill entire available space
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
        self.verticalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)

    def setData(self, df: pd.DataFrame):
        self.clear()
        self.setRowCount(len(df.index))
        self.setColumnCount(len(df.columns))
        self.setHorizontalHeaderLabels(df.columns)

        # getting data from df is computationally costly so convert it to array first
        df_array = df.values
        for row in range(df.shape[0]):
            self.setItem(row, 0, QTableWidgetItem(str(df_array[row][0])))
            self.setItem(row, 1, QTableWidgetItem(str(df_array[row][1])))
            self.setItem(row, 2, QTableWidgetItem(str(round(df_array[row][2], 3))))


class TableMplWidget(QtWidgets.QGroupBox):

    def __init__(self, dfName, *args, **kwargs):
        QtWidgets.QGroupBox.__init__(self, dfName, *args, **kwargs)
        self.table = TableView()
        self.mplWidget = MplWidget()
        grpLayout = QtWidgets.QVBoxLayout()
        grpLayout.addWidget(self.table)
        grpLayout.addWidget(self.mplWidget)

        self.setLayout(grpLayout)

    def setData(self, df: pd.DataFrame, xAxisName):
        self.mplWidget.setData(df, xAxisName)
        self.table.setData(df)


class ClonalityzerGui(QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.__view = Ui_mwClonalityzer()
        self.__view.setupUi(self)
        self.setWindowIcon(QtGui.QIcon(r"GUI/icons/antibody.png"))

        self.__view.actionImport_File.triggered.connect(lambda: self.__showOpenFileDialog())
        self.__view.actionExport_Results.triggered.connect(lambda: self.__showSaveFileDialog())
        self.currentWorkingDir = ""
        self.__imporFileChanged = None
        self.__exportFileChanged = None

    def setImportFileChangedListener(self, func):
        self.__imporFileChanged = func

    def setExportFileChangedListener(self, func):
        self.__exportFileChanged = func

    def setData(self, genesData: dict, clonesData: dict, basicAnalysis: pd.DataFrame):
        #clear layouts
        self.__deleteLayout(self.__view.tpCDR.layout())
        self.__deleteLayout(self.__view.tpVDJ.layout())
        self.__deleteLayout(self.__view.tpFunctionality.layout())
        #create new layouts
        genesLayout = self.__createLayout(genesData, "Gene")
        self.__view.tpVDJ.setLayout(genesLayout)
        clonesLayout = self.__createLayout(clonesData, "Sequences")
        self.__view.tpCDR.setLayout(clonesLayout)
        functLayout = self.__createLayoutOfFunctionalityTab(basicAnalysis)
        self.__view.tpFunctionality.setLayout(functLayout)
        self.__view.actionExport_Results.setEnabled(True)  # only then saving results is possible

    def __createLayoutOfFunctionalityTab(self, basicAnalysis: pd.DataFrame):
        layout = QGridLayout()
        table = TableView()
        table.setData(basicAnalysis)
        layout.addWidget(table, 0, 0)
        lblTotalRead = QLabel()
        lblTotalRead.setText("Valid Genes/Read Genes: " + str(
            basicAnalysis.iloc[:3].sum()["Count"]) + "/" + str(sum(basicAnalysis["Count"])))
        lblTotalRead.setAlignment(QtCore.Qt.AlignRight)
        layout.addWidget(lblTotalRead, 1, 0)
        plot = MplWidget()
        plot.setColoredData(basicAnalysis, "Functionality")
        layout.addWidget(plot, 2, 0)
        return layout

    def __createLayout(self, dataframes: dict, xAxisName):
        layout = QHBoxLayout()
        for dfName in dataframes.keys():
            dataView = TableMplWidget(dfName)
            dataView.setData(dataframes[dfName], xAxisName)
            layout.addWidget(dataView)
        return layout

    def __deleteLayout(self, layout: QLayout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    layout.deleteLayout(item.layout())
            delete(layout)

    def __showOpenFileDialog(self):
        zipFile = QFileDialog.getOpenFileName(caption="Open Zip File... ", filter="Zip Files (*.zip)")[0]
        if zipFile != "":
            item, ok = QInputDialog().getItem(self, "Select Sequences for Analysis", "Analysis based on: ",
                                              ["3_Nt-Sequences", "5_AA-Sequences"], 0, False)
            if ok and item:
                self.__imporFileChanged(zipFile, item)

    def __showSaveFileDialog(self):
        filename = QFileDialog.getSaveFileName(caption="Save Results to... ", filter="CSV Files (*.csv *.txt)")[0]
        if filename != "":
            self.__exportFileChanged(filename)

# generate file from ui use following command:
# pyuic5 -o ClonalityzerGUI_Designer.py GUI.ui