import matplotlib.ticker as mtick
import matplotlib
import pandas as pd

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QVBoxLayout

matplotlib.use('Qt5Agg')


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        fig.set_tight_layout(True)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class MplWidget(QtWidgets.QWidget):

    def __init__(self, *args, **kwargs):
        QtWidgets.QWidget.__init__(self, *args, **kwargs)
        self.canvas = MplCanvas()

        toolbar = NavigationToolbar(self.canvas, self)  # Create toolbar of pyplot
        self.layout = QVBoxLayout()
        self.layout.addWidget(toolbar)
        self.layout.addWidget(self.canvas)
        self.setLayout(self.layout)
        self.setAutoFillBackground(True)

    def setData(self, df: pd.DataFrame, xAxisName: str):
        self.canvas.axes.cla()  # Clear the canvas.
        ax = df.plot(kind="bar", x=xAxisName, y="Percentage", legend=False, ax=self.canvas.axes, rot=90)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1))

        self.canvas.draw()

    def setColoredData(self, df: pd.DataFrame, xAxisName: str):
        self.canvas.axes.cla()  # Clear the canvas.
        ax = df.plot(kind="bar", x=xAxisName, y="Percentage", legend=False, ax=self.canvas.axes, rot=0,
                     color=['indianred', 'dodgerblue', 'green', 'mediumpurple'])
        ax.yaxis.set_major_formatter(mtick.PercentFormatter(1))

        self.canvas.draw()
