from Logic.Statistics import Statistics
from GUI.View.ClonalityzerGUI import ClonalityzerGui

from zipfile import ZipFile
import glob
import os


class VDJController:

    def start(self):
        self.model = None
        self.view = ClonalityzerGui()
        self.view.setImportFileChangedListener(self.calculateStatistics)
        self.view.setExportFileChangedListener(self.saveStatistics)
        self.view.show()

    def saveStatistics(self, filename):
        self.model.printStatisticsToFile(filename)

    def calculateStatistics(self, zipFile, sequences):
        self.model = Statistics()
        filename = self.__unzipFolder(zipFile, sequences)
        self.model.createStatistics(filename)
        self.genesAnalysis = self.model.genes
        self.model.printStatisticsConsole()
        viewGenesFiltered = self.filterDataByTreshold(self.genesAnalysis)
        viewClonesFiltered = self.filterDataByTreshold(self.model.clones)
        self.view.setData(viewGenesFiltered, viewClonesFiltered, self.model.functionalities)

    def __unzipFolder(self, zipFile, sequences):
        with ZipFile(zipFile, 'r') as zipObj:
            # create name of future directory with data
            zipFilename = os.path.basename(zipFile)
            self.currentWorkingDir = os.path.join(os.getcwd(), "__pycache__", zipFilename.replace(".zip", "_temp"))

            # Extract all the contents of zip file
            zipObj.extractall(self.currentWorkingDir)
            filename = glob.glob(self.currentWorkingDir + "./*/" + sequences + "*.txt")[0]
            return filename

    def filterDataByTreshold(self, dataframes: dict):
        filteredDataframes = dict()
        for dfName in dataframes.keys():
            df = dataframes[dfName]
            filteredDataframes[dfName] = df.nlargest(20, "Count")
        return filteredDataframes

