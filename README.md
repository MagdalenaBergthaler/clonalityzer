# Clonalityzer

The adaptive immune system is significantly involved in the defence of pathogens. Two of its key roles are B and T cells. They recognize and bind antigens with their receptors -- BCR and TCR -- and invoke other defence mechanisms.
The analysis of these receptors leads to a better knowledge of the adaptive immune system. In the area of organ transplantations, they help to detect transplant rejections and react in an early stage.
Clonalityzer provides general analysis of BCRs and TCRs based on data pre-processed by IMGT/HighV-Quest. Clonalityzer analyses the frequency of sequences as well as of VDJ genes and clonality. The results are displayed on a graphical user interface and can be saved in CSV format for further analysis.

## Software Requirements
Clonalityzer runs on a Python Version 3.7+ downloadable from here https://www.python.org/downloads/
In addition it needs Qt5 (on linux)

## Installation
following packages need to be installed:
```bash
pip install PyQt5 matplotlib pandas
```

## Usage
After all packages are installed, doubleclick the main.py and a graphical user interface should pop up.
You can now start your analysis and import your first dataset via File>Import Data

## References
The logo of Clonalityzer was adapted from https://www.flaticon.com/authors/ddara