import os
import re
import pandas as pd


class Statistics:

    def __init__(self):
        self.seq_total = 0
        self.__genesV = {}
        self.__genesD = {}
        self.__genesJ = {}
        self.genes = {"V Genes": self.__genesV, "D Genes": self.__genesD, "J Genes": self.__genesJ}
        self.clones = {"CDR1": {}, "CDR2": {}, "CDR3": {}}
        self.functionalities = {"productive": 0, "unproductive": 0, "unknown": 0, "no results": 0}

    def __addPercentagesToFunctionality(self):
        totalResultSeqs = self.functionalities.iloc[:3].sum()["Count"]
        self.functionalities['Percentage'] = self.functionalities["Count"] / totalResultSeqs

        # no result has its own calculation since it will not be considered for other statistics
        totalSeqs = sum(self.functionalities["Count"])
        self.functionalities.loc[3, 'Percentage'] = self.functionalities.loc[3, "Count"] / totalSeqs

        return totalResultSeqs

    def __addPercentagesToDataFrame(self, df):
        totalSeqs = sum(df["Count"])
        df['Percentage'] = df["Count"] / totalSeqs

    def __count_VDJ_genes(self, gene_columns):
        gene = set(re.findall(r"([VJD][0-9]+(-[0-9]+)*)", gene_columns,
                              re.MULTILINE))  # if multiple genes are associated to sequence it gets all of tem and removes dublicates
        if gene is not None:  # j2-2P can be counted to j2-2
            for g in gene:
                genes = {}
                if g[0][0] == "V":  # choose correct dictionary of the genes
                    genes = self.__genesV
                elif g[0][0] == "J":
                    genes = self.__genesJ
                elif g[0][0] == "D":
                    genes = self.__genesD

                if genes.get(g[0]) is None:
                    genes[g[0]] = 0
                genes[g[0]] += 1

    def __addCountClones(self, line_content):
        if len(line_content) >= 10:
            self.__countCDRX("CDR1", line_content[10])
            if len(line_content) >= 12:
                self.__countCDRX("CDR2", line_content[12])
                if len(line_content) >= 14:
                    self.__countCDRX("CDR3", line_content[14])

    def __countCDRX(self, cdr_name, cdr_column):
        cdr_dict = self.clones[cdr_name]
        if cdr_column != "" and cdr_column != "null":
            if cdr_column not in cdr_dict:
                cdr_dict[cdr_column] = 0
            cdr_dict[cdr_column] += 1

    def createStatistics(self, filename: str):
        self.__readFile(filename)
        self.__calcRemainingStats()

    def __readFile(self, filename):
        with open(filename) as file:
            header = file.readline()
            for line in file.readlines():
                self.seq_total += 1
                line = line.replace("\n", "")
                line_content = line.split("\t")
                functionality = line_content[2].lower()
                self.functionalities[functionality] += 1

                # count vdj-genes and clones
                gene_columns = ""
                if (len(line_content) > 3):
                    gene_columns += line_content[3]
                    if (len(line_content) > 4):
                        gene_columns += line_content[4]
                        if (len(line_content) > 5):
                            gene_columns += line_content[5]
                            self.__addCountClones(line_content)
                    self.__count_VDJ_genes(gene_columns)

    def __convertToDataframesAndAddPercentages(self, dictionaries, columns):
        for k in dictionaries.keys():
            dictionaries[k] = pd.DataFrame(dictionaries[k].items(), columns=columns)
            self.__addPercentagesToDataFrame(dictionaries[k])

    def __printDictOfDataframes(self, dfDict):
        for k in dfDict.keys():
            print(k + ":")
            print(dfDict[k].sort_values("Count", ascending=False))

    def __calcRemainingStats(self):

        self.__convertToDataframesAndAddPercentages(self.genes, ["Gene", "Count"])
        self.__convertToDataframesAndAddPercentages(self.clones, ["Sequences", "Count"])

        self.functionalities = pd.DataFrame(self.functionalities.items(), columns=["Functionality", "Count"])
        self.__addPercentagesToFunctionality()

    def printStatisticsConsole(self):
        print(self.clones["CDR1"].sort_values("Count", ascending=False))
        print("Total number of clones " + str(sum(self.clones["CDR1"]["Count"])))
        print(self.__genesV)
        print(self.__genesD)
        print(self.__genesJ)
        print()
        print(self.functionalities)
        print("Total number of Sequences: " + str(self.seq_total))  # result contains all but no results
        self.__printDictOfDataframes(self.clones)

    def printStatisticsToFile(self, filename: str):
        fileInfo = os.path.splitext(filename)
        for dfName in self.clones.keys():
            newFilename = fileInfo[0] + "_" + dfName + fileInfo[1]
            self.clones[dfName].to_csv(newFilename, index=False, sep=";")

        for dfName in self.genes.keys():
            newFilename = fileInfo[0] + "_" + dfName + fileInfo[1]
            self.genes[dfName].to_csv(newFilename, index=False, sep=";")

        self.functionalities.to_csv(fileInfo[0] + "_functionality" + fileInfo[1], index=False, sep=";")
