import sys

from PyQt5 import QtWidgets
from GUI.Controller.VDJController import VDJController

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    controller = VDJController()
    controller.start()
    sys.exit(app.exec_())
